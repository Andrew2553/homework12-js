// Подія input запускається щоразу після того, як користувач змінює значення. На відміну від подій клавіатури, input запускається при будь-якій зміні значень, навіть тих, які не передбачають дії клавіатури: вставлення тексту за допомогою миші або використання розпізнавання мовлення для диктування тексту.

const buttons = document.querySelectorAll('.btn');

document.addEventListener('keyup', (event) => {
  const pressedKey = event.key.toUpperCase();
  buttons.forEach((button) => {
    if (button.innerText.toUpperCase() === pressedKey) {
      button.style.backgroundColor = 'blue';
      button.style.color = 'white';
    } else {
      button.style.backgroundColor = 'white';
      button.style.color = 'black';
    }
  });
});
